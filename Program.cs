﻿using System;
using System.Collections.Generic;

// Общий интерфейс для автомобиля
public interface ICar
{
    string GetDescription();
}

// Конкретная реализация автомобиля
public class Car : ICar
{
    private string color;
    private string transmission;

    public Car(string color, string transmission)
    {
        this.color = color;
        this.transmission = transmission;
    }

    public string GetDescription()
    {
        return $"Параметры автомобиля:\nColor: {color}, \nTransmission: {transmission}\nadditionally:\n";
    }
}

public abstract class CarFactory
{
    public abstract ICar CreateCar();
}

// Конкретная фабрика для создания седана
public class SedanCarFactory : CarFactory
{
    public override ICar CreateCar()
    {
        return new Car("Blue", "MT");
    }
}

// Конкретная фабрика для создания купе
public class CoupeCarFactory : CarFactory
{
    public override ICar CreateCar()
    {
        return new Car("Red", "AT");
    }
}

// Абстрактный класс декоратора автомобиля
public abstract class CarDecorator : ICar
{
    protected ICar car;

    public CarDecorator(ICar car)
    {
        this.car = car;
    }

    public virtual string GetDescription()
    {
        return car.GetDescription();
    }
}

// Конкретный декоратор для добавления дополнительного параметра (Кондиционер)
public class ConditionerDecorator : CarDecorator
{
    public ConditionerDecorator(ICar car) : base(car)
    {
    }

    public override string GetDescription()
    {
        string description = base.GetDescription();
        description += "conditioner";
        return description;
    }
}

// Класс итератора
public interface ICarIterator
{
    bool HasNext();
    ICar Next();
}

// Класс коллекции автомобилей
public class CarCollection
{
    private List<ICar> cars = new List<ICar>();

    public void AddCar(ICar car)
    {
        cars.Add(car);
    }

// Интерфейс итератора
    public ICarIterator GetIterator()
    {
        return new CarIterator(this);
    }

// Итератор
    private class CarIterator : ICarIterator
    {
        private CarCollection collection;
        private int position = 0;

        public CarIterator(CarCollection collection)
        {
            this.collection = collection;
        }

        public bool HasNext()
        {
            return position < collection.cars.Count;
        }

        public ICar Next()
        {
            ICar car = collection.cars[position];
            position++;
            return car;
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        CarCollection carCollection = new CarCollection();

        // Создание фабрик и объектов седанов
        CarFactory sedanCarFactory = new SedanCarFactory();
        ICar sedanCar = sedanCarFactory.CreateCar();

        // Создание фабрик и объектов купе
        CarFactory coupeCarFactory = new CoupeCarFactory();
        ICar coupeCar = coupeCarFactory.CreateCar();

        // Добавление автомобилей в коллекцию
        carCollection.AddCar(sedanCar);
        carCollection.AddCar(coupeCar);

        // Применение декоратора
        ICar decoratedCar = new ConditionerDecorator(sedanCar);
        carCollection.AddCar(decoratedCar);

        // Применение итератора
        ICarIterator iterator = carCollection.GetIterator();
        while (iterator.HasNext())
        {
            ICar car = iterator.Next();
            Console.WriteLine(car.GetDescription());
        }
    }
}

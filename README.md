# Практическая работа №2 Маханько Денис БСБО-11-21


## О программе:

Существует завод по производству автомобилей. При этом на нем существуют разделение производства седанов и купе. Для каждого типа автомобиля созданы фабрики (SedanCarFactory и CoupeCarFactory), которые отвечают за производство соответствующих автомобилей.
Фабричный метод используется для создания экземпляров автомобилей определенного типа. Каждая фабрика реализует метод CreateCar(), который создает и возвращает новый экземпляр автомобиля своего типа.

Для добавления дополнительной функциональности к автомобилям используется паттерн Декоратор. Например, создается декоратор ConditionerDecorator, который добавляет кондиционер к автомобилю.
Декораторы могут быть применены к автомобилям, созданным фабриками, чтобы добавить дополнительные параметры и функции.
Собранные автомобили хранятся в коллекции на заводе.

Для перебора и обработки автомобилей в коллекции используется паттерн Итератор. Создается итератор, который позволяет последовательно обращаться к каждому автомобилю в коллекции.
Завод может использовать итератор для вывода описания каждого автомобиля и его параметров.

## Фабричный метод

Этот паттерн используется для создания объектов определенного типа, но с возможностью варьировать конкретный класс, который будет создан. Он определяет интерфейс для создания объекта, но оставляет подклассам решение о конкретном классе, который следует создать. Таким образом, мы можем создавать объекты, не указывая конкретный класс напрямую, а через фабричные методы.

## Декоратор

Этот паттерн предоставляет способ последовательного доступа к элементам коллекции без раскрытия деталей ее реализации. Он позволяет нам обходить элементы коллекции и выполнять определенные операции над ними, не зная конкретной структуры коллекции. Итератор абстрагирует доступ к элементам и предоставляет единый интерфейс для работы с ними, что делает код более гибким и независимым от конкретной реализации коллекции.

## Итератор

Это поведенческий паттерн проектирования, который даёт возможность последовательно обходить элементы составных объектов, не раскрывая их внутреннего представления.

Вместе эти паттерны позволяют нам создавать объекты с использованием фабричного метода, добавлять дополнительные возможности и параметры с помощью декораторов, а затем последовательно просматривать их с помощью итератора. Это обеспечивает гибкость, расширяемость и удобство в работе с объектами и их функциональностью.

## Диаграмма классов
```plantuml
@startuml

interface ICar {
    +GetDescription(): string
}

class Car {
    -color: string
    -transmission: string
    +Car(color: string, transmission: string)
    +GetDescription(): string
}

abstract class CarFactory {
    +CreateCar(): ICar
}

class SedanCarFactory {
    +CreateCar(): ICar
}

class CoupeCarFactory {
    +CreateCar(): ICar
}

abstract class CarDecorator {
    -car: ICar
    +CarDecorator(car: ICar)
    +GetDescription(): string
}

class ConditionerDecorator {
    +ConditionerDecorator(car: ICar)
    +GetDescription(): string
}

interface ICarIterator {
    +HasNext(): bool
    +Next(): ICar
}

class CarCollection {
    -cars: List<ICar>
    +AddCar(car: ICar): void
    +GetIterator(): ICarIterator
}

class CarIterator {
    -collection: CarCollection
    -position: int
    +CarIterator(collection: CarCollection)
    +HasNext(): bool
    +Next(): ICar
}

class Program {
    +Main(args: string[]): void <<entrypoint>>
}

Program --> CarCollection
Program --> SedanCarFactory
Program --> CoupeCarFactory
Program --> ConditionerDecorator

CarCollection o-- ICarIterator
CarCollection "1" *-- "*" ICar
CarIterator ..> CarCollection
CarIterator --> ICarIterator
SedanCarFactory --|> CarFactory
CoupeCarFactory --|> CarFactory
Car --> ICar
CarDecorator --|> ICar
ConditionerDecorator --|> CarDecorator

@enduml



```
